require 'open-uri'
require "Nokogiri"
require 'pp'


selectors = ["student_name_selector = h1", 
    "tagline_selector = section#about h2", 
    "student_intro_paragraph_selector = section#about p:nth(1)", 
    "aspirations_content_selector = section#about p:nth(2)",
    "interests_selector = section#about p:nth(3)", 
    "work_history_selector = div.one_half:nth(1) li",
    "education_history_selector = div.one_half:nth(2) li", 
    "favorite_apps_selector = section#favorites figcaption", 
    "favorite_sites_selector = section#favorites div.columns"]
links = [
    "social_media_icons_selector = div.social_icons a",
    "social1 = social_icons a",
    "social2 = social_icons a",
    "github_link_selector = tr:nth(1) td:nth(1) a", 
    "treehouse_link_selector = tr:nth(1) td:nth(2) a",
    "codeschool_link_selector = tr:nth(2) td:nth(1) a", 
    "coderwall_link_selector = tr:nth(2) td:nth(1) a",
    "faves1 = #favorites div.columns:nth(1) a",
    "faves2 = #favorites div.columns:nth(2) a",
    "faves3 = #favorites div.columns:nth(3) a" ]

url = "http://students.flatironschool.com"
doc = Nokogiri::HTML.parse(open(url))
people = doc.css("div.one_third a").map { |link| link['href'] }    #obtains href text
people.each_with_index do |array|
  begin
    url = "http://students.flatironschool.com/#{array}" 
    doc = Nokogiri::HTML(open(url))
  rescue
    next
  end  
    about_section = {}
    selectors.each do |selector|
        key = selector.split(" = ").first
        tag = selector.split(" = ").last
        about_section[key.to_sym] = doc.css(tag).text
    end
    links = {}
    links.each do |link|
        key = selector.split(" = ").first
        tag = selector.split(" = ").last
        links[key.to_sym] = doc.css(tag).attr("href").text
    end
    pp about_section
    pp links
end

# doc.css("section#about div.one_third img").first["src"]       # THIS IS FOR THE MAIN PIC, NEED THIS



# url = "http://students.flatironschool.com/masha.html"
# doc = Nokogiri::HTML(open(url))
# doc.css("#favorites div.columns:nth(1) a").attr("href")

# social_links = social_links_list.map do |social_link|
#   social_link.attr("href")


# social_links_selector = ".social_icons a"
# work_links_selector = "section#former_life div.one_half:nth(1) li a"
# edu_link_selector = "section#former_life div.one_half.last ul li a"
# blog_link_selector = ".columns.coder_cred div p a"
# company_names_selector = "section#favorites figcaption"
# fav_company_links_selector = "#favorites div.columns:nth(1) a"
# fav_websites_selector = "#favorites .one_third p"
# fav_websites_links_selector = "#favorites div.columns:nth(2) a"
# quotes_selector = ".one_fourth p"