require 'open-uri'
require "Nokogiri"
require 'pp'

#define path to desired data via css selector
student_name_selector = "h1" #Works fine
tagline_selector = "section#about h2" #Works
student_intro_paragraph_selector = "section#about p:nth(1)"
aspirations_content_selector = "section#about p:nth(2)"
interests_selector = "section#about p:nth(3)"
social_media_icons_selector = "div.social_icons a" #Just gets the raw links, not icons
work_history_selector = "div.one_half:nth(1) li"
education_history_selector = "div.one_half:nth(2) li"
github_link_selector = "tr:nth(1) td:nth(1) a" #Link
treehouse_link_selector = "tr:nth(1) td:nth(2) a" #Link
codeschool_link_selector = "tr:nth(2) td:nth(1) a" #Link
coderwall_link_selector = "tr:nth(2) td:nth(1) a" #Link
favorite_apps_selector = "section#favorites figcaption" #This takes the text of the app name, not a link
favorite_sites_selector = "section#favorites div.columns"

# instantiate empty hash for data collection
student_data = {}
student_data_links = {}


# open index file, obtain url for each profile, create people array from html links
# iterate through each profile in people array, obtain desired data (unless link is broken)

url = "http://students.flatironschool.com"
doc = Nokogiri::HTML.parse(open("#{url}"))
people = doc.css("div.one_third a").map { |link| link['href'] }    #obtains href text
people.each_with_index do |array|
  begin
    url = "http://students.flatironschool.com/#{array}" 
    doc = Nokogiri::HTML(open("#{url}"))
  rescue
    next
  end  
    student_data = {                                                #populate student data hash, given defined key
     :student_name => doc.css(student_name_selector).text,
     :tagline => doc.css(tagline_selector).text,
     :student_intro_paragraph =>
    doc.css(student_intro_paragraph_selector).text,
     :aspirations => doc.css(aspirations_content_selector).text,
     :interests => doc.css("#{interests_selector}").text,
     :work_history => doc.css("#{work_history_selector}").text,
     :education_history => doc.css("#{education_history_selector}").text,
     :favorite_apps => doc.css("#{favorite_apps_selector}").text,
     :favorite_sites => doc.css("#{favorite_sites_selector}").text }
    doc = Nokogiri::HTML.parse(open("#{url}"))
    student_data_links = {
     :social_media_links => doc.css("#{social_media_icons_selector}").map{ |link| link['href'] },
     :github_link => doc.css("#{github_link_selector}").map { |link| link['href'] },
     :treehouse_link => doc.css("#{treehouse_link_selector}").map { |link|link['href'] },
     :codeschool_link => doc.css("#{codeschool_link_selector}").map {|link| link['href'] },
     :coderwall_link => doc.css("#{coderwall_link_selector}").map { |link|link['href'] }
        }
    pp student_data
    pp student_data_links
end








require 'open-uri'
require "Nokogiri"
require 'pp'


selectors = ["student_name_selector = h1", "tagline_selector = section#about h2", 
    "student_intro_paragraph_selector = section#about p:nth(1)"]

about_section = {}
url = "http://students.flatironschool.com/masha.html" 
doc = Nokogiri::HTML(open("url"))
selectors.each do |selector|
    key = selector.split(" = ").first
    tag = selector.split(" = ").last
    about_section[key.to_sym] = doc.css(tag).text
end

    


