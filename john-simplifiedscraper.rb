#nokogiri
#
#First define all selectors
#go through each selector ad
require 'rubygems'
require 'open-uri'
require 'nokogiri'
require 'pp'

url = "http://students.flatironschool.com/johnkellyferguson.html"

student_name_selector = "h1" #Works
tagline_selector = "section#about h2" #Works
aspirations_content_selector = "section#about p:nth(2)" #Works
interests_selector = "section#about p:nth(3)" #Works
work_history_selector = "div.one_half:nth(1) li" #Works
education_history_selector = "div.one_half:nth(2) li" #Works

####Broken
favorite_sites_selector = "section#favorites div.columns" #Strange Formatting (Extra Spacing), Includes App Name and description

# student_intro_paragraph_selector = "section#about p:nth(1)" #Broken
# social_media_icons_selector = "div.social_icons a" #Just gets the raw links, not ids
# github_link_selector = "tr:nth(1) td:nth(1) a" #Link
# treehouse_link_selector = "tr:nth(1) td:nth(2) a" #Link
# codeschool_link_selector = "tr:nth(2) td:nth(1) a" #Link
# coderwall_link_selector = "tr:nth(2) td:nth(1) a" #Link
# blog_post_selector = ""
# favorite_apps_selector = "section#favorites figcaption" #This takes the text of the appname, not a link

# all_quotes_selector = ""

hash = {}
selector_array = [student_name_selector, tagline_selector, aspirations_content_selector, interests_selector, work_history_selector, education_history_selector, favorite_sites_selector]
keys_array = [:name, :tagline, :aspirations, :interests, :work, :education, :favoritesites]

# (1..2).each do |i|

# selector_array.each_with_index do |array|
#   doc = Nokogiri::HTML(open("#{url}"))
#   doc.css("#{array}").each do |i|

#     #hash[array.to_s.to_sym] = {}
#   end
# end

values = []

def get_value(url, selector)
  doc = Nokogiri::HTML(open("#{url}"))
  doc.css("#{selector}").each do |element|
    return element.content.to_s
  end
end

selector_array.each do |element|
  url = "http://students.flatironschool.com/johnkellyferguson.html"
  value = get_value(url, element)
  values << value
end

puts values


# selector_array.each_with_index do |hash|
#   doc = Nokogiri::HTML(open("#{url}"))
#   doc.css("#{hash}").each do |hash|
#     hash = {:name => selector_array[0].content.to_s.to_sym}
#     hash = {:tagline => tagline_selector.content.to_s.to_sym}
#     hash = {:intro => student_intro_paragraph_selector.content.to_s.to_sym}