require 'open-uri'
require 'nokogiri'

# scrape one student profile
student_url = 'http://students.flatironschool.com/anabecker.html'

student_name_selector = "div.two_third h1"
student_name = student_doc.css(student_name_selector).first.content

student_img_selector = "section#about div.one_third img"
student_img = student_doc.css(student_img_selector).first['src']

student_tagline_selector = "div.two_third h2"
student_tagline = student_doc.css(student_tagline_selector).first.content

student_bio_selector = "div.two_third h2 + p"
# student_bio = student_doc.css(student_tagline_selector).next_element.content
student_bio = student_doc.css(student_bio_selector).first.content

h3_selectors = "div.two_third h3"
h3_p_selectors = "div.two_third h3 + p"

h3_in_about = student_doc.css(h3_selectors)

h3_titles = h3_in_about.collect{|h3| h3.content}


ps_in_h3_in_about = student_doc.css(h3_p_selectors)

h3_p_content = ps_in_h3_in_about.collect{|p| p.content}
h3_p_content = ps_in_h3_in_about.collect(&:content)

# about_student = Hash[*h3_titles.zip(h3_p_content).flatten]

about_student = {}
h3_titles.each_with_index do |h3_content, index|
  about_student[h3_content.to_sym] = h3_p_content[index]
end

# blog_link_selector = "i.icon-globe"
# blog_link = student_doc.css(blog_link_selector).first.parent['href'] if student_doc.css(blog_link_selector).first

social_links_selector = "div.social_icons i"
social_link_elements = student_doc.css(social_links_selector)

social_links = []

# for every i in social links
social_link_elements.each do |i_element|
  # get the class of the i and strip off the icon-
  link_type = i_element['class'].gsub("icon-", "")
  # then go up to the parent a and get the href
  link_href = i_element.parent['href']
  # group those together as a key value pair
  social_links << {link_type.to_sym => link_href}
end