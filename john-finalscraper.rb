require 'open-uri'
require "Nokogiri"
require "sqlite3"
require 'pp'

database = SQLite3::Database.new( "john-scrape.db" )
database.execute( "CREATE TABLE IF NOT EXISTSstudents (id INTEGER PRIMARY KEY, name TEXT, tagline TEXT, bio TEXT);" )



url = "http://students.flatironschool.com"
doc = Nokogiri::HTML.parse(open(url))
people = doc.css("div.one_third a").map { |link| link['href'] }    #obtains href text
people.each_with_index do |array|
  begin
    url = "http://students.flatironschool.com/#{array}" 
    doc = Nokogiri::HTML(open(url))
    #Student Name
    student_name_selector = "div.two_third h1"
    student_name = doc.css(student_name_selector).first.content
    #Student Tagline
    student_tagline_selector = "div.two_third h2"
    student_tagline = doc.css(student_tagline_selector).first.content
    #Student Bio
    student_bio_selector = "div.two_third h2 + p"
    student_bio = doc.css(student_bio_selector).first.content
  rescue
    next
  end  
  database.execute( "insert into students 
    (name, tagline, bio) 
    values 
    (?, ?, ?)", [student_name, student_tagline, student_bio])
      #'#{student_name}', '#{student_tagline}', '#{student_bio}')")
end

rows = database.execute( "select * from students" )
p rows

# db.execute("INSERT INTO students (name, email, grade, blog) 
#             VALUES (?, ?, ?, ?)", [@name, @email, @grade, @blog])

# #Student Image
# student_img_selector = "section#about div.one_third img"
# student_img = student_doc.css(student_img_selector).first['src']

# # #Aspirations & Interests
# # h3_selectors = "div.two_third h3"
# # h3_p_selectors = "div.two_third h3 + p"

# # h3_in_about = student_doc.css(h3_selectors)
# # student_aspiration_interests_titles = h3_in_about.collect{|h3| h3.content}

# # ps_in_h3_in_about = student_doc.css(h3_p_selectors)

# # student_aspiration_interests = ps_in_h3_in_about.collect{|p| p.content}

# # about_student = {}
# # student_aspiration_interests_titles.each_with_index do |student_aspiration_interests, index|
# #   about_student[student_aspiration_interests.to_sym] = student_aspiration_interests[index]
# # end

# # puts about_student